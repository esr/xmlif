<!DOCTYPE refentry PUBLIC 
   "-//OASIS//DTD DocBook XML V4.1.2//EN"
   "docbook/docbookx.dtd">
<refentry id='xmlif.1'>
<refmeta>
<refentrytitle>xmlif</refentrytitle>
<manvolnum>1</manvolnum>
<refmiscinfo class='date'>Oct 04 2004</refmiscinfo>
<refmiscinfo class='productname'>xmlif</refmiscinfo>
<refmiscinfo class='source'>xmlif</refmiscinfo>
<refmiscinfo class='manual'>XML</refmiscinfo>
</refmeta>
<refnamediv id='name'>
<refname>xmlif</refname>
<refpurpose>conditional processing instructions for XML</refpurpose>
</refnamediv>
<refsynopsisdiv id='synopsis'>

<cmdsynopsis>
  <command>xmlif</command>
  <arg>--help</arg>
  <arg>--version</arg>
  <arg rep='repeat'>attrib=value</arg>
</cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='options'><title>OPTIONS</title>
<variablelist>
  <varlistentry>
  <term>--help</term>
  <listitem><para>Emit a usage summary and exit.</para></listitem>
  </varlistentry>
  <varlistentry>
  <term>--version</term>
  <listitem><para>Emit the program version and exit.</para></listitem>
  </varlistentry>
</variablelist>
</refsect1>
<refsect1 id='description'><title>DESCRIPTION</title>

<para><application>xmlif</application> filters XML according to
conditionalizing markup.  This can be useful for formatting one of several
versions of an XML document depending on conditions passed to the
command.</para>

<para>Attribute/value pairs from the command line are matched against
the attributes associated with certain processing instructions in the
document.  The instructions are <sgmltag>&lt;?maybe if?&gt;</sgmltag>
and its inverse <sgmltag>&lt;?maybe if not?&gt;</sgmltag>,
<sgmltag>&lt;?maybe elif?&gt;</sgmltag> and its inverse
<sgmltag>&lt;?maybe elif not?&gt;</sgmltag>, <sgmltag>&lt;?maybe
else?&gt;</sgmltag>, and <sgmltag>&lt;?maybe fi?&gt;</sgmltag>.</para>

<para>Argument/value pairs given on the command line are checked
against the value of corresponding attributes in the conditional
processing instructions.  An &lsquo;attribute match&rsquo; happens if
an attribute occurs in both the command-line arguments and the tag,
and the values match.  An &lsquo;attribute mismatch&rsquo; happens if
an attribute occurs in both the command-line arguments and the tag,
but the values do not match.</para>

<para>Spans between <sgmltag>&lt;?maybe if?&gt;</sgmltag> or
<sgmltag>&lt;?maybe elif?&gt;</sgmltag> and the next conditional
processing instruction at the same nesting level are passed through
unaltered if there is at least one attribute match and no attribute
mismatch; spans between <sgmltag>&lt;?maybe if not?&gt;</sgmltag> and
<sgmltag>&lt;?maybe elif not?&gt;</sgmltag> and the next conditional
processing instruction are passed otherwise.  Spans between
<sgmltag>&lt;?maybe else?&gt;</sgmltag> and the next
conditional-processing tag are passed through only if no previous span
at the same level has been passed through. <sgmltag>&lt;?maybe
if?&gt;</sgmltag> and <sgmltag>&lt;?maybe fi?&gt;</sgmltag> (and their
&lsquo;not&rsquo; variants) change the current nesting level;
<sgmltag>&lt;?maybe else?&gt;</sgmltag> and <sgmltag>&lt;?maybe
elif?&gt;</sgmltag> do not.</para>

<para>Conditionals can be nested to any depth.</para>

<para>All these processing instructions will be removed from the output
produced. Aside from the conditionalization, all other input is passed
through untouched; in particular, entity references are not resolved.</para>

<para>Value matching is by string equality, except that "|"
in an attribute value is interpreted as an alternation
character.  Thus, saying foo='red|blue' on the command line enables 
conditions red and blue.  Saying color='black|white' in a tag matches 
command-line conditions color='black' or color='white'.</para>

<para>Here is an example:</para>

<programlisting>
Always issue this text.
&lt;?maybe if condition='html'?&gt;
Issue this text if 'condition=html' is given on the command line.
&lt;?maybe elif condition='pdf|ps'?&gt;
Issue this text if 'condition=pdf' or 'condition=ps'
is given on the command line.
&lt;?maybe else?&gt;
Otherwise issue this text.
&lt;?maybe fi?&gt;
Always issue this text.
</programlisting>

</refsect1>

<refsect1 id='warning'><title>WARNING</title>
<para>Thanks to a previously-unsuspected landmine in the depths of the
XML standard, it was necessary to change the processing instruction
from 'xmlif' to 'maybe' in release 1.2 of this tool.  This is an
incompatible change from previous versions, The management regrets any
inconvenience.</para>
</refsect1>

<refsect1 id='authors'><title>AUTHOR</title>
<para>Eric S. Raymond &lt;esr@snark.thyrsus.com&gt;.</para>
</refsect1>
</refentry>

