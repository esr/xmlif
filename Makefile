# xmlif project

VERS=$(shell sed -n <NEWS '/^[0-9]/s/:.*//p' | head -1)

CFLAGS = -O -DVERSION='"${VERS}"'

all: xmlif xmlif.1

xmlif: xmlif.c Makefile
	$(CC) $(CFLAGS) -o xmlif xmlif.c 

xmlif.c: xmlif.l
	flex -oxmlif.c xmlif.l

xmlif.1: xmlif.xml
	xmlto man xmlif.xml

check:
	@$(MAKE) -C tests --quiet check BINDIR=$(realpath $(CURDIR))

install: xmlif.1 uninstall
	cp xmlif /usr/bin
	cp xmlif.1 /usr/share/man/man1/xmlif.1

uninstall:
	rm -f /usr/bin/xmlif /usr/share/man/man1/xmlif.1

clean:
	rm -f xmlif xmlif.1 xmlif.c xmlif-*.rpm xmlif-*.tar.gz *~
	rm -f *.html *.tar.gz

SOURCES = README COPYING NEWS control xmlif.l Makefile xmlif.xml test.xml

xmlif-$(VERS).tar.gz: $(SOURCES) xmlif.1
	@ls $(SOURCES) xmlif.1 | sed s:^:xmlif-$(VERS)/: >MANIFEST
	@(cd ..; ln -s xmlif xmlif-$(VERS))
	(cd ..; tar -czvf xmlif/xmlif-$(VERS).tar.gz `cat xmlif/MANIFEST`)
	@(cd ..; rm xmlif-$(VERS))

dist: xmlif-$(VERS).tar.gz

xmlif.html: xmlif.xml
	xmlto xhtml-nochunks xmlif.xml

release: xmlif-$(VERS).tar.gz xmlif.html
	shipper version=$(VERS) | sh -e -x

refresh: xmlif.html
	shipper -N -w version=$(VERS) | sh -e -x

